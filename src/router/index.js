import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Resume from "../views/Resume.vue";
import Projects from "../views/Projects.vue";
import caseStudy from "../views/caseStudy.vue";

Vue.use(VueRouter);

const routes = [
  { path: "/", redirect: "/home" },
  { path: "/home", component: Home, name: "Home" },
  { path: "/resume", component: Resume, name: "Resume" },
  { path: "/projects", component: Projects, name: "Projects" },
  { path: "/caseStudy", component: caseStudy, name: "caseStudy" }
];

const router = new VueRouter({
  routes
});

export default router;
